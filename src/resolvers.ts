import { User, Post } from ".prisma/client";
import * as Context from "./context"
import { PubSub } from 'graphql-subscriptions';

const pubsub = new PubSub();

export const resolvers = {
    Query: {
        allUsers: (_parent: any, _args: any, context: Context.Context) => {
            return context.prisma.user.findMany();
        },
        user: (_parent: any, args: { id: number }, context: Context.Context) => {
            return context.prisma.user.findUnique({
                where: {
                    id: args.id
                }
            })
        },
        allPosts: (_: any, __: any, context: Context.Context) => {
            return context.prisma.post.findMany();
        },
        post: (_: any, _args: { id: number }, context: Context.Context) => {
            return context.prisma.post.findUnique({
                where: {
                    id: _args.id
                }
            })
        },
    },

    Mutation: {
        addUser: (_parent: any, _args: { name: string }, context: Context.Context) => {
            return context.prisma.user.create({ data: { name: _args.name } })
        },
        createPost: (_parent: any, _args: { input: { userId: number, content: string } }, context: Context.Context) => {
            const post = context.prisma.post.create({
                data: {
                    authorId: _args.input.userId,
                    content: _args.input.content,
                }
            })
            pubsub.publish('POST_CREATED', { postCreated: post });
            return post;
        }
    },

    Subscription: {
        postCreated: {
            subscribe: () => pubsub.asyncIterator(['POST_CREATED']),
        },
    },

    User: {
        posts: (_parent: User, _args: any, context: Context.Context) => {
            if (Object.keys(context).length === 0) {
                context = Context.context;
            }
            return context.prisma.post.findMany({
                where: {
                    authorId: _parent.id
                }
            })
        }

    },

    Post: {
        author: (_parent: Post, _args: any, context: Context.Context) => {
            if (Object.keys(context).length === 0) {
                context = Context.context;
            }
            return context.prisma.user.findUnique({
                where: { id: _parent.authorId }
            });
        }
    }
}