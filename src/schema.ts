import { gql } from 'apollo-server-core'

export const typeDefs = gql`
    type Mutation {
        "Add a user to the DB"
        addUser(name: String!): User
        "Create a post as the given user"
        createPost(input: CreatePostInput): Post
    }

    type Query {
        "Fetch all users"
        allUsers: [User!]
        "Fetch a specific User by ID"
        user(id: Int!) : User
        allPosts: [Post!]
        post(id: Int!) : Post
    }

    type Subscription {
        "Notify when any post is created"
        postCreated: Post
    }
    
    type User {
        "ID of the user in the db"
        id: Int!
        "Name of the user"
        name: String!
        "The posts that a user has made"
        posts: [Post!]
    }

    type Post {
        "ID of a post"
        id: Int!
        "The timestamp when the post was created"
        createdAt: String!
        "The timestamp when the post was last updated"
        updatedAt: String!
        "The content of the post"
        content: String!
        "The author of the post"
        author: User!
        "ID of the author of the post"
        authorId: Int
    }

    input CreatePostInput {
        userId: Int!
        content: String!
    }
`