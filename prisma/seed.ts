import { PrismaClient, Prisma } from '@prisma/client'

const prisma = new PrismaClient()

const userData: Prisma.UserCreateInput[] = [
    {
        name: 'Harry',
        posts: {
            create: [
                {
                    content: `Here's some text`,
                },
            ],
        },
    },
    {
        name: 'Geoff',
        posts: {
            create: [
                {
                    content: `Here's some more content`,
                },
            ],
        },
    },
    {
        name: 'Horatia',
        posts: {
            create: [
                {
                    content: 'Yet another thing',
                    color: '#FACADE'
                },
                {
                    content: 'One with some color',
                    color: '#DECAFF'
                },
            ],
        },
    },
]

async function main() {
    console.log(`Start seeding ...`)
    for (const u of userData) {
        const user = await prisma.user.create({
            data: u,
        })
        console.log(`Created user with id: ${user.id}`)
    }
    console.log(`Seeding finished.`)
}

main()
    .catch((e) => {
        console.error(e)
        process.exit(1)
    })
    .finally(async () => {
        await prisma.$disconnect()
    })